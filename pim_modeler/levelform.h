#ifndef LEVELFORM_H
#define LEVELFORM_H

#include <QWidget>
#include "layer.h"
#include <QFileDialog>
#include <geometryes/geometryimage.h>



namespace Ui {
class LevelForm;
}

class LevelForm : public QWidget
{
    Q_OBJECT

public:
    explicit LevelForm(Layer *layer, QWidget *parent = 0);
    ~LevelForm();

    Layer *layer() const;

private:
    Ui::LevelForm *ui;
    Layer *_layer;
};

#endif // LEVELFORM_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <dialogs/startdialog.h>
#include "plot.h"
#include "layer.h"
#include <QDebug>
#include <QModelIndex>
#include <QLabel>
#include <QFileDialog>
#include "levelform.h"
#include "geometryes/geometrypoint.h"
#include "geometryes/geometrypolygon.h"


#define EXP 0.000001

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum create : int {
        CREATE_NULL = 1,
        CREATE_TEST = 2,
        CREATE_ROOM = 3
    };

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void createNewFile();

private:

    void selectCreateType(int type = CREATE_NULL);

    // статусбар
    QLabel *_coordsStatus;

    // инициализация

    void initAll();

    void initLevelAddWidget();

    void initPlot();

    // стартовый диалог
    StartDialog *_startDialog;


    // плоскость рисования
    Plot *_plot;

    // слои
    void addNew();

    qreal _zoom = 1;

    Ui::MainWindow *ui;

    int _createComand = 0;
    // рабочая геометрия
    Geometry *_curentGeometry = nullptr;
    // выделенная геометрия
    Geometry *_tmpSelected = nullptr;


    QPen _selectedGeometryPen;
    QPen _roomGeometryPen;
    QBrush _selectedGeometryBrush;
    QBrush _roomGeometryBrush;

};

#endif // MAINWINDOW_H



QT       += core gui widgets

CONFIG += c++11

TARGET = pim_modeler
TEMPLATE = app

SOURCES += \
    main.cpp\
    mainwindow.cpp \
    dialogs/startdialog.cpp \
    levelform.cpp \

HEADERS  += mainwindow.h \
    dialogs/startdialog.h \
    levelform.h \


FORMS    += mainwindow.ui \
    dialogs/startdialog.ui \
    levelform.ui


RESOURCES += \
    res.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libmodeler/release/ -llibmodeler
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libmodeler/debug/ -llibmodeler
else:unix: LIBS += -L$$OUT_PWD/../libmodeler/ -llibmodeler

INCLUDEPATH += $$PWD/../libmodeler
DEPENDPATH += $$PWD/../libmodeler

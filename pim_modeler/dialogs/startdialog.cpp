#include "startdialog.h"
#include "ui_startdialog.h"

StartDialog::StartDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartDialog)
{
    ui->setupUi(this);
    setModal(true);

    connect(ui->cancelPushButton, &QPushButton::clicked, this, &StartDialog::close);
    connect(ui->createNewFilePushButton, &QPushButton::clicked, this, &StartDialog::close);
    connect(ui->openNewFormatPushButton, &QPushButton::clicked, this, &StartDialog::close);
    connect(ui->openOldFilePushButton, &QPushButton::clicked, this, &StartDialog::close);

    connect(ui->cancelPushButton, &QPushButton::clicked, this, &StartDialog::exit);
    connect(ui->createNewFilePushButton, &QPushButton::clicked, this, &StartDialog::newFile);
    connect(ui->openNewFormatPushButton, &QPushButton::clicked, this, &StartDialog::openNew);
    connect(ui->openOldFilePushButton, &QPushButton::clicked, this, &StartDialog::openOld);

}

StartDialog::~StartDialog()
{
    delete ui;
}

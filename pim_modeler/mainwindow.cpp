#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("PIM-modeler v.3");

    _startDialog = new StartDialog(this);
    connect(_startDialog, &StartDialog::exit, this, &MainWindow::close);
    connect(_startDialog, &StartDialog::newFile, this, &MainWindow::createNewFile);


    connect(ui->levelAddPushButton, &QPushButton::clicked, this, &MainWindow::addNew);



    ui->toolBox->removeItem(0);
    _coordsStatus = new QLabel();
    ui->statusBar->addWidget(_coordsStatus);


    _selectedGeometryPen.setColor(QColor(30, 160, 200));
    _selectedGeometryPen.setWidth(2);

    _roomGeometryPen.setColor(QColor(200, 0, 0));
    _roomGeometryPen.setWidth(1);

    _selectedGeometryBrush.setColor(QColor(30, 160, 200));
    _selectedGeometryBrush.setStyle(Qt::FDiagPattern);

    _roomGeometryBrush.setColor(QColor(200, 0, 0));
    _roomGeometryBrush.setStyle(Qt::FDiagPattern);

    selectCreateType();

    _startDialog->show();
}

MainWindow::~MainWindow()
{
    delete _startDialog;
    delete ui;
}

void MainWindow::createNewFile()
{
    initAll();
}

void MainWindow::selectCreateType(int type)
{
    _createComand = type;
    ui->pushButtonAddRoom->setStyleSheet("background-color:white");
    ui->pushButtonAddZone->setStyleSheet("background-color:white");
    ui->pushButtonHoles->setStyleSheet("background-color:white");
    ui->pushButtonTest->setStyleSheet("background-color:white");
    switch (_createComand) {
    case CREATE_ROOM:
        ui->pushButtonAddRoom->setStyleSheet("background-color: rgb(42, 200, 60)");
        break;
    case CREATE_TEST:
        ui->pushButtonTest->setStyleSheet("background-color: rgb(42, 200, 60)");
    default:
        break;
    }
}

void MainWindow::initAll()
{
    initPlot();
    initLevelAddWidget();
}

void MainWindow::initLevelAddWidget()
{

}

void MainWindow::initPlot()
{
    _plot = new Plot();
    //_plot->show();
    ui->plotContent->addWidget(_plot);

    connect(_plot, &Plot::curentCoords, [this](QPointF p){
        _coordsStatus->setText(QString(tr("Coords: [%1, %2]").arg(p.x()).arg(p.y())));
    });

    ui->levelAddPushButton->setEnabled(true);
    ui->levelDoubleSpinBox->setEnabled(true);

    connect(ui->levelAddPushButton, &QPushButton::clicked, [this](){
        addNew();
    });

    connect(_plot, &Plot::newLayer, [this](Layer *l) {
        LevelForm *lf = new LevelForm(l, ui->toolBox);
        ui->toolBox->addItem(lf, l->layerName());
    });

    connect(ui->pushButtonZoomMinux, &QPushButton::clicked, [this](){
        _zoom = (_zoom < 1) ? _zoom - 0.1 : _zoom - 1;
        _zoom = (_zoom <= 0.1) ? 0.1 : _zoom;
        _plot->setZoom(_zoom);
    });
    connect(ui->pushButtonZoomNull, &QPushButton::clicked, [this](){
        _zoom = 1;
        _plot->setZoom(_zoom);
    });
    connect(ui->pushButtonZoomPlus, &QPushButton::clicked, [this](){
        _zoom = (_zoom < 1) ? _zoom + 0.1 : _zoom + 1;
        //_zoom = (_zoom >= 0.1) ? 0.1 : _zoom;
        _plot->setZoom(_zoom);
    });
    connect(ui->pushButtonTest, &QPushButton::clicked, [this](){
        selectCreateType(CREATE_TEST);
    });
    connect(ui->pushButtonAddRoom, &QPushButton::clicked, [this](){
        selectCreateType(CREATE_ROOM);
    });
    connect(_plot, &Plot::clickCoords, [this](QPointF p, int modifier) {
        if (modifier & Plot::CLICK_MODE_CTRL) { // create point
            if (_curentGeometry == nullptr) { // создание
                switch (_createComand) {
                case CREATE_TEST: {
                    GeometryPoint *point = new GeometryPoint("point", p);
                    Layer *current = dynamic_cast<LevelForm *>(ui->toolBox->currentWidget())->layer();
                    current->addGeometry(point, point->defaultRange());
                    break;
                }
                case CREATE_ROOM: {
                    _curentGeometry = new GeometryPolygon("polygon");
                    _curentGeometry->setPen(_selectedGeometryPen);
                    _curentGeometry->setBrush(_selectedGeometryBrush);
                    Layer *current = dynamic_cast<LevelForm *>(ui->toolBox->currentWidget())->layer();
                    current->addGeometry(_curentGeometry, _curentGeometry->defaultRange());
                    break;
                }
                case CREATE_NULL:
                default:
                    break;
                }
            }
            if (_curentGeometry != nullptr) {
            // редактирование
                if (dynamic_cast<GeometryPolygon *>(_curentGeometry)) {
                    GeometryPolygon *polygon = dynamic_cast<GeometryPolygon *>(_curentGeometry);
                    polygon->addPoint(p);
                } else if (dynamic_cast<GeometryPoint *>(_curentGeometry)) {
                    GeometryPoint *polygon = dynamic_cast<GeometryPoint *>(_curentGeometry);
                    polygon->setPosition(p);
                    emit polygon->updated();
                }
                _plot->update();
            }


        } else if (modifier & Plot::CLICK_MODE_RIGHT &&
                   !(modifier & Plot::CLICK_MODE_CTRL)){ // end create
            /*switch (_createComand) {
            case CREATE_TEST:

                break;
            case CREATE_ROOM:
                if (_curentPolygon) {
                    _curentPolygon->setBrush(_roomGeometryBrush);
                    _curentPolygon->setPen(_roomGeometryPen);
                    _curentPolygon = nullptr;
                }
                break;
            case CREATE_NULL:
            default:
                break;
            }*/
            if (_curentGeometry) {
                _curentGeometry->setBrush(_roomGeometryBrush);
                _curentGeometry->setPen(_roomGeometryPen);
                _curentGeometry = nullptr;
            }
        } else {
            if (_tmpSelected) {
                if (_curentGeometry) {
                    _curentGeometry->setBrush(_roomGeometryBrush);
                    _curentGeometry->setPen(_roomGeometryPen);
                    _curentGeometry = nullptr;
                }
                _curentGeometry = _tmpSelected;
                _curentGeometry->setPen(_selectedGeometryPen);
                _curentGeometry->setBrush(_selectedGeometryBrush);
                //emit _curentGeometry->updated();
                _plot->update();

            }
        }
    });

    connect(_plot, &Plot::selectedGeometryes, [this](QList<Geometry *> g) {
        for (Geometry *geo : g)
            qDebug() << geo;
        if (!g.isEmpty()) {
            _tmpSelected = g.first();
            //qDebug() << "TMP";
        } else {
            _tmpSelected = nullptr;
        }
    });
}


void MainWindow::addNew()
{
    if (_plot){
        Layer *layer = new Layer(QString("Level%1").arg(ui->levelDoubleSpinBox->value()),
                                 ui->levelDoubleSpinBox->value(), _plot);
        _plot->addLayer(layer);
    }
}

#include "levelform.h"
#include "ui_levelform.h"
#include <QDebug>


LevelForm::LevelForm(Layer *layer, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LevelForm)
{
    _layer = layer;
    ui->setupUi(this);

    connect(ui->checkBoxUseBackgroundImage, &QCheckBox::toggled, [this](bool c){
        ui->pushButtonOpenBackgroundImage->setEnabled(c);
    });
    connect(ui->pushButtonOpenBackgroundImage, &QPushButton::clicked, [this](){
        QString imageName = QFileDialog::getOpenFileName(0, tr("Open backgroundImage"), "", "*.jpg");
        if (!imageName.isEmpty()){
            GeometryImage *image = new GeometryImage(
                        _layer->layerName() + QString("_geometryImage_backgroundImage"),
                        QPixmap(imageName),
                        QPointF(0, 0),
                        _layer);
            _layer->addGeometry(image, 0);
            emit _layer->updated();
        }
    });

}

LevelForm::~LevelForm()
{
    delete ui;
}

Layer *LevelForm::layer() const
{
    return _layer;
}

#include "geometrypoint.h"

GeometryPoint::GeometryPoint(const QString &name,const QPointF &position, QObject *parent):
    Geometry(name, parent)
{
    _geometryName = name;
    _position = position;
}

void GeometryPoint::draw(QPainter *painter, Viewport *viewport)
{
    QPoint pos = viewport->metricToPixel(_position);
    painter->setPen(_pen);
    painter->drawPoint(pos);
    painter->drawEllipse(pos, 5, 5);
}

bool GeometryPoint::inside(const QPointF &p, Viewport *viewport)
{
    QPoint pos = viewport->metricToPixel(_position);
    QPointF lt = viewport->pixelToMetric(
                QPoint(pos.x() - 5, pos.y() - 5));
    QPointF rb = viewport->pixelToMetric(
                QPoint(pos.x() + 5, pos.y() + 5));
    QRectF rect(lt, rb);
    return rect.contains(p);
}

QPointF GeometryPoint::position() const
{
    return _position;
}

void GeometryPoint::setPosition(const QPointF &position)
{
    _position = position;
    emit updated();
}

int GeometryPoint::defaultRange()
{
    return 100;
}

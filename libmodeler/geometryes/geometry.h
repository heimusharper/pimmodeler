#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <QObject>
#include <QPainter>
#include "../viewport.h"

class Geometry : public QObject
{
    Q_OBJECT
public:
    explicit Geometry(QString _name, QObject *parent = 0);

    virtual void draw(QPainter *painter, Viewport *viewport) = 0;
    QString getName(){
        return _geometryName;
    }
    virtual bool isDrawable() const {
        return _isDrawable;
    }
    virtual void setIsDrawable(bool isDrawable) {
        _isDrawable = isDrawable;
    }
    virtual bool inside(const QPointF &p, Viewport *viewport) = 0;

    virtual int defaultRange() {
        return 0;
    }
    virtual void setPen(const QPen &pen) final {
        _pen = pen;
        emit updated();
    }
    virtual void setBrush(const QBrush &brush) final {
        _brush = brush;
        emit updated();
    }

protected:

    bool _isDrawable = true;
    QString _geometryName = "";
    QPen _pen;
    QBrush _brush;

signals:

    void updated();

public slots:
};

#endif // GEOMETRY_H

#ifndef GEOMETRYPOINT_H
#define GEOMETRYPOINT_H
#include "geometry.h"


class GeometryPoint : public Geometry
{
    Q_OBJECT
public:
    explicit GeometryPoint(const QString &name, const QPointF &position, QObject *parent = 0);

    void draw(QPainter *painter, Viewport *viewport) override;

    bool inside(const QPointF &p, Viewport *viewport) override;

    QPointF position() const;
    void setPosition(const QPointF &position);

    int defaultRange() override;

private:

    QPointF _position;

};

#endif // GEOMETRYPOINT_H

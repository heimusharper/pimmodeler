#ifndef GEOMETRYIMAGE_H
#define GEOMETRYIMAGE_H
#include "geometry.h"
#include <QDebug>

class GeometryImage : public Geometry
{
    Q_OBJECT
public:
    explicit GeometryImage(const QString &name, QPixmap pixmap, const QPointF &position, QObject *parent = 0);

    void draw(QPainter *painter, Viewport *viewport) override;

    bool inside(const QPointF &p, Viewport *viewport) override;

    int defaultRange() override;

private:

    QImage _drawablePixmapSource;
    QImage _drawablePixmapScaled;
    qreal _scalesZoom = -1;
    QPointF _position;

};

#endif // GEOMETRYIMAGE_H

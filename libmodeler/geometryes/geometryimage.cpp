#include "geometryimage.h"

GeometryImage::GeometryImage(const QString &name, QPixmap pixmap, const QPointF &position, QObject *parent) :
    Geometry(name, parent)
{
    _drawablePixmapSource = pixmap.toImage();
    _position = position;

}

void GeometryImage::draw(QPainter *painter, Viewport *viewport)
{

    if (_scalesZoom == -1){
        _scalesZoom = viewport->getZoom();
        _drawablePixmapScaled = _drawablePixmapSource.scaled(
                    viewport->getZoom() * _drawablePixmapSource.width(),
                    viewport->getZoom() * _drawablePixmapSource.height());
    } else {
        if (!(_scalesZoom + 0.00001 > viewport->getZoom() &&
                _scalesZoom - 0.00001 < viewport->getZoom())) {
            _scalesZoom = viewport->getZoom();

            _drawablePixmapScaled = _drawablePixmapSource.scaled(
                        viewport->getZoom() * _drawablePixmapSource.width(),
                        viewport->getZoom() * _drawablePixmapSource.height());
        }
    }
    QPoint pos = viewport->metricToPixel(_position);
    painter->drawImage(
                QPoint(pos.x() - _drawablePixmapScaled.width()/2, pos.y()
                       - _drawablePixmapScaled.height()/2),
                _drawablePixmapScaled);
}

bool GeometryImage::inside(const QPointF &p, Viewport *viewport)
{
    QPoint center = viewport->metricToPixel(_position);
    QPointF lt = viewport->pixelToMetric(
                QPoint(
                    center.x() - _drawablePixmapScaled.width()/2,
                    center.y() - _drawablePixmapScaled.width()/2));
    QPointF rb = viewport->pixelToMetric(
                QPoint(
                    center.x() + _drawablePixmapScaled.width()/2,
                    center.y() + _drawablePixmapScaled.width()/2));

    QRectF rect(lt, rb);
    return rect.contains(p);
}

int GeometryImage::defaultRange()
{
    return 999;
}

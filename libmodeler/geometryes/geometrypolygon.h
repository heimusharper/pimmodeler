#ifndef GEOMETRYPOLYGON_H
#define GEOMETRYPOLYGON_H
#include "geometry.h"
#include <QPen>
#include <QBrush>
#include "geometrypoint.h"

class GeometryPolygon : public Geometry
{
    Q_OBJECT
public:
    GeometryPolygon(const QString &name, QObject *parent = 0);

    void draw(QPainter *painter, Viewport *viewport) override;
    bool inside(const QPointF &p, Viewport *viewport) override;


    void setPoints(const QList<QPointF *> &points);
    int addPoint(const QPointF &point);

    int defaultRange() override;

    QList<GeometryPoint *> points();

private:



    QList<GeometryPoint *> _points;

};

#endif // GEOMETRYPOLYGON_H

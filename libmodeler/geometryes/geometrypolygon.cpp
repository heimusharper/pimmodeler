#include "geometrypolygon.h"

GeometryPolygon::GeometryPolygon(const QString &name, QObject *parent) :
    Geometry(name, parent)
{
    _geometryName = name;
}

void GeometryPolygon::draw(QPainter *painter, Viewport *viewport)
{
    painter->setPen(_pen);
    painter->setBrush(_brush);

    QPoint lastPoint = viewport->metricToPixel(_points.first()->position());
    QPoint pos_px;
    QPoint first = lastPoint;
    /*for (GeometryPoint *point : _points){
        pos_px = viewport->metricToPixel( point->position());
        painter->drawLine(lastPoint.x(), lastPoint.y(),
                          pos_px.x(), pos_px.y());
        lastPoint = pos_px;
    }
    painter->drawPolygon(poly);
*/
    QPolygonF poly;
    for (GeometryPoint *p : _points){
        poly.append(viewport->metricToPixel(p->position()));

    }
    painter->drawPolygon(poly);

    /*painter->drawLine(lastPoint.x(), lastPoint.y(),
                      first.x(), first.y());
*/
    for (GeometryPoint *point : _points) {
        /*point->setPen(_pen);
        point->setBrush(_brush);*/
        point->draw(painter, viewport);
    }
}

bool GeometryPolygon::inside(const QPointF &p, Viewport *viewport)
{

    QPolygonF poly;
    for (GeometryPoint *p : _points){
        poly.append(p->position());
    }
    return poly.containsPoint(p, Qt::OddEvenFill);

}

void GeometryPolygon::setPoints(const QList<QPointF *> &points)
{
    _points.clear();
    for (QPointF *p : points) {
        GeometryPoint *newPoint = new GeometryPoint("point", *p);
        //connect(newPoint, &GeometryPoint::updated, this, &GeometryPolygon::updated);
        /*newPoint->setPen(_pen);
        newPoint->setBrush(_brush);*/
        _points.append(newPoint);
    }
    emit updated();
}


int GeometryPolygon::addPoint(const QPointF &point)
{
    GeometryPoint *p = new GeometryPoint("point", point);
    //connect(p, &GeometryPoint::updated, this, &GeometryPolygon::updated);
    _points.append(p);
    emit updated();
    return _points.size()-1;
}

int GeometryPolygon::defaultRange()
{
    return 100;
}

QList<GeometryPoint *> GeometryPolygon::points()
{
    return _points;
}

#ifndef VIEWPORT_H
#define VIEWPORT_H
#include <QRectF>
#include <QPointF>
#include <QPoint>
#include <QObject>

class Viewport : public QObject
{
    Q_OBJECT
public:
    Viewport(const QSize &viewportsize, QObject *parent = 0);


    /*!
     * \brief getViewportRectF
     * \return
     * метрические координаты видимой области рисования
     */
    QRectF getViewportRectF();
    /*!
     * \brief metricToPixel
     * \param point
     * \return
     * переводит координаты из метрических в координаты области рисования
     */
    QPoint metricToPixel(const QPointF &point);
    /*!
     * \brief pixelToMetric
     * \param point
     * \return
     * переводит координаты из координат области рисования в метрические
     */
    QPointF pixelToMetric(const QPoint &point);

    qreal getZoom() const;
    void setZoom(const qreal &zoom);

    /*!
     * \brief getCenter
     * \return
     * центр -- точка в метрических координатах, которой соответствует
     * точка (0,0) в координатах области рисования
     * |-----------|
     * |           |
     * |           |
     * |     +     |-
     * |           | y
     * |         x |-
     * |-----------|
     *       | x |
     * x -- метрические (0,0)
     * + -- (0,0) дисплея
     */
    QPointF getCenter() const;
    void setCenter(const QPointF &center);
    /*!
     * \brief getViewportSize
     * \return
     * размер области рисования - размер полотна
     */
    QSize getViewportSize() const;
    void setViewportSize(const QSize &viewportSize);

private:

    qreal _zoom = 1;
    QPointF _center;
    QSize _viewportSize;

};

#endif // VIEWPORT_H

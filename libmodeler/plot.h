#ifndef PLOT_H
#define PLOT_H

#include <QWidget>
#include "layer.h"
#include <QMap>
#include <QHBoxLayout>
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include "viewport.h"
#include <QWheelEvent>

class Plot : public QWidget
{
    Q_OBJECT
public:

    enum clickModes : quint32 {
        CLICK_MODE_LEFT = 1,
        CLICK_MODE_RIGHT = 2,
        CLICK_MODE_CTRL = 4,
        CLICK_MODE_SHIFT = 8,
        CLICK_MODE_ALT = 16
    };

    explicit Plot(QWidget *parent = 0);

    void addLayer(Layer *layer);
    void removeLayer(Layer *layer);
    void showLayer(Layer *layer);

    void update();

    QList<Layer *> layers() const;
    Layer *layer(qreal l);

    double zoom() const;
    void setZoom(double zoom);

    Viewport *viewport() const;

    void setGrid(int grid);

private:

    QList<Layer *> _layers;
    QHBoxLayout *_rootLayout;
    QPoint _lastPos;
    int _grid = 10;
    bool _pressed = false;
    QPixmap _plot;

    Viewport *_viewport;

protected:

    void paintEvent(QPaintEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void resizeEvent(QResizeEvent *event);
    void wheelEvent(QWheelEvent *event);

signals:

    void onAddLayer(double level);
    void onRemoveLayer(double level);
    void curentCoords(QPointF p);
    void clickCoords(QPointF p, quint32 mode);
    void newLayer(Layer *layer);
    void selectedGeometryes(QList<Geometry *> geom);

public slots:
};

#endif // PLOT_H

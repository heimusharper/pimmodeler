#-------------------------------------------------
#
# Project created by QtCreator 2016-06-28T22:40:02
#
#-------------------------------------------------

QT       += core gui widgets

CONFIG += c++11

TARGET = libmodeler
TEMPLATE = lib


SOURCES += \
    plot.cpp \
    geometryes/geometry.cpp \
    layer.cpp \
    geometryes/geometryimage.cpp \
    viewport.cpp \
    geometryes/geometrypolygon.cpp \
    geometryes/geometrypoint.cpp

HEADERS  += \
    plot.h \
    geometryes/geometry.h \
    layer.h \
    geometryes/geometryimage.h \
    viewport.h \
    geometryes/geometrypolygon.h \
    geometryes/geometrypoint.h


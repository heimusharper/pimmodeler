#include "viewport.h"

Viewport::Viewport(const QSize &viewportsize, QObject *parent) :
    QObject(parent)
{
    setViewportSize(viewportsize);
    setCenter(QPointF(0, 0));
}

QRectF Viewport::getViewportRectF()
{

}

QPoint Viewport::metricToPixel(const QPointF &point)
{
    qreal pixelsAtMeter = _zoom;

    int x_px = (point.x() - _center.x()) * pixelsAtMeter;
    int y_px = (point.y() - _center.y()) * pixelsAtMeter;



    return QPoint(_viewportSize.width()/2 + x_px, _viewportSize.height()/2 - y_px);
}

QPointF Viewport::pixelToMetric(const QPoint &point)
{
    qreal metersAtPixel = 1.0/_zoom;

    qreal x_m = _viewportSize.width()/2.0 - point.x();
    qreal y_m = _viewportSize.height()/2.0 - point.y();

    return QPointF( - x_m * metersAtPixel + _center.x(),
                     y_m * metersAtPixel + _center.y());

}

qreal Viewport::getZoom() const
{
    return _zoom;
}

void Viewport::setZoom(const qreal &zoom)
{
    _zoom = zoom;
}

QPointF Viewport::getCenter() const
{
    return _center;
}

void Viewport::setCenter(const QPointF &center)
{
    _center = center;
}

QSize Viewport::getViewportSize() const
{
    return _viewportSize;
}

void Viewport::setViewportSize(const QSize &viewportSize)
{
    _viewportSize = viewportSize;
}

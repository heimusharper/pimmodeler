#include "plot.h"

Plot::Plot(QWidget *parent) : QWidget(parent)
{
    _rootLayout = new QHBoxLayout();
    setLayout(_rootLayout);
    _viewport = new Viewport(this->size());

    setMouseTracking(true);

}

void Plot::addLayer(Layer *layer)
{
    _layers.append(layer);
    onAddLayer(layer->levelValue());
    connect(layer, &Layer::updated, [this](){
        repaint();
    });
    emit newLayer(layer);
}

void Plot::removeLayer(Layer *layer)
{
    _layers.removeAt(_layers.indexOf(layer));
    onRemoveLayer(layer->levelValue());
}

void Plot::showLayer(Layer *layer)
{

}

void Plot::update()
{
    repaint();
}

QList<Layer *> Plot::layers() const
{
    return _layers;
}

Layer *Plot::layer(qreal l)
{
    for (Layer *lay : _layers){
        if (lay->levelValue() + 0.0001 > l &&
                lay->levelValue() - 0.0001 < l) {
            return lay;
        }
    }
    return nullptr;
}

double Plot::zoom() const
{
    return _viewport->getZoom();
}

void Plot::setZoom(double zoom)
{
    _viewport->setZoom(zoom);
    repaint();
}


Viewport *Plot::viewport() const
{
    return _viewport;
}

void Plot::setGrid(int grid)
{
    _grid = grid;
}



void Plot::paintEvent(QPaintEvent *e)
{
    QPainter p(this);

    p.drawRect(0, 0, this->width()-1, this->height()-1);

    QWidget::paintEvent(e);

    for (Layer *layer : _layers) {
        layer->draw(&p, _viewport);
    }

    p.setPen(QPen());

    p.drawLine(_viewport->metricToPixel(QPointF(0, 0)),
               QPoint(
                   _viewport->metricToPixel(QPointF(0, 0)).x(),
                   -this->height()));

    p.drawLine(_viewport->metricToPixel(QPointF(0, 0)),
               QPoint(
                   _viewport->metricToPixel(QPointF(0, 0)).x(),
                   this->height()));

    p.drawLine(_viewport->metricToPixel(QPointF(0, 0)),
               QPoint(
                   this->width(),
                   _viewport->metricToPixel(QPointF(0, 0)).y()));

    p.drawLine(_viewport->metricToPixel(QPointF(0, 0)),
               QPoint(
                   -this->width(),
                   _viewport->metricToPixel(QPointF(0, 0)).y()));

    int l = _viewport->metricToPixel(QPointF(0, 0)).x() -
            _viewport->metricToPixel(QPointF(_grid, 0)).x();
    l = std::abs(l);
    if (_grid > 0) {
        p.setPen(QPen(QColor(0, 0, 0, 50)));
        QPoint center = _viewport->metricToPixel(QPoint(0, 0));
        for (int i = center.x(); i < this->width(); i+=l){
            p.drawLine(i, this->height(), i, 0);
        }
        for (int i = center.x(); i > 0; i-=l) {
            p.drawLine(i, this->height(), i, 0);
        }
        for (int i = center.y(); i < this->height(); i+=l) {
            p.drawLine(0, i, this->width(), i);
        }
        for (int i = center.y(); i > 0; i -=l) {
            p.drawLine(0, i, this->width(), i);
        }
    }
}

void Plot::mouseMoveEvent(QMouseEvent *e)
{
    emit curentCoords(_viewport->pixelToMetric(e->pos()));
    qDebug() << "(0,0) " << _viewport->pixelToMetric(QPoint(0, 0)) << _viewport->getCenter();
    if (_pressed) {
        QPoint p;
        p.setX(_viewport->metricToPixel(_viewport->getCenter()).x()
               + _lastPos.x() - e->pos().x());
        p.setY(_viewport->metricToPixel(_viewport->getCenter()).y()
               + _lastPos.y() - e->pos().y() );
        //p = _viewport->pixelToMetric(e->pos());
        _viewport->setCenter(_viewport->pixelToMetric(p));
        _lastPos = e->pos();
        repaint();
    }
    QWidget::mouseMoveEvent(e);
}

void Plot::mousePressEvent(QMouseEvent *e)
{
    _lastPos = e->pos();





    quint32 mode = 0;

    switch (e->button()) {
    case Qt::LeftButton:
        mode |= CLICK_MODE_LEFT;
        break;
    case Qt::RightButton:
        mode |= CLICK_MODE_RIGHT;
        break;
    default:
        break;
    }

    switch (e->modifiers()) {
    case Qt::ControlModifier:
        mode |= CLICK_MODE_CTRL;
        break;
    case Qt::ShiftModifier:
        mode |= CLICK_MODE_SHIFT;
        break;
    case Qt::AltModifier:
        mode |= CLICK_MODE_ALT;
    default:
        break;
    }


    QPointF pos = _viewport->pixelToMetric(e->pos());
    QList<Geometry *> geometryes;
    for (Layer *l : _layers) {
        geometryes.append(l->inside(pos, _viewport));
    }

    if (mode == CLICK_MODE_LEFT) {
        _pressed = true;
    }


    emit selectedGeometryes(geometryes);
    emit clickCoords(viewport()->pixelToMetric(_lastPos), mode);
}

void Plot::mouseReleaseEvent(QMouseEvent *e)
{
    _pressed = false;
}

void Plot::resizeEvent(QResizeEvent *event)
{
    _viewport->setViewportSize(event->size());
}

void Plot::wheelEvent(QWheelEvent *event)
{
    qreal dz = (event->delta()>0) ? 1 : -1;
    if (_viewport->getZoom() <= 1.1)
        dz *= 0.1;
    _viewport->setZoom(_viewport->getZoom() + dz);
    update();
}

#ifndef LAYER_H
#define LAYER_H

#include <QObject>
#include "geometryes/geometry.h"
#include "viewport.h"
#include <QMap>
#include "geometryes/geometrypolygon.h"

class Layer : public QObject
{
    Q_OBJECT
public:
    explicit Layer(QString layerName, double level, QObject *parent = 0);


    double levelValue() const;
    void setLevelValue(double levelValue);

    void draw(QPainter *painter, Viewport *viewport);

    void addGeometry(Geometry *geometry, int range);
    void removeGeometry(Geometry *geometry);

    QList<Geometry *> inside(const QPointF &p, Viewport *viewport);
    QString layerName() const;

private:

    QMap< int, QList<Geometry *> *> _geometryes;
    double _levelValue = 0;
    QString _layerName = "";

signals:

    void updated();

public slots:
};

#endif // LAYER_H

#include "layer.h"

Layer::Layer(QString layerName, double level, QObject *parent) : QObject(parent)
{
    setLevelValue(level);
    _layerName = layerName;

}

double Layer::levelValue() const
{
    return _levelValue;
}

void Layer::setLevelValue(double levelValue)
{
    _levelValue = levelValue;
}

void Layer::draw(QPainter *painter, Viewport *viewport)
{
    for (int i : _geometryes.keys()) {
        for (Geometry *geometry : *_geometryes.value(i)) {
            geometry->draw(painter, viewport);
        }
    }
}

void Layer::addGeometry(Geometry *geometry, int range)
{
    if (_geometryes.value(range)){
        _geometryes.value(range)->append(geometry);
        connect(geometry, &Geometry::updated, [this](){
            emit updated();
        });
    } else {
        _geometryes.insert(range, new QList<Geometry *>());
        addGeometry(geometry, range);
        return;
    }
}

void Layer::removeGeometry(Geometry *geometry)
{
    for (int i : _geometryes.keys())
    _geometryes.value(i)->removeAt(_geometryes.value(i)->indexOf(geometry));
}

QList<Geometry *> Layer::inside(const QPointF &p, Viewport *viewport)
{
    QList<Geometry *> geometryes;
    for (int i : _geometryes.keys()) {
        for (Geometry *g : *_geometryes.value(i)) {
            if (g->inside(p, viewport))
                geometryes.append(g);
            if (dynamic_cast<GeometryPolygon*>(g)) {
                QList<GeometryPoint *> points = dynamic_cast<GeometryPolygon*>(g)->points();
                for (GeometryPoint *point : points)
                    if (point->inside(p, viewport))
                        geometryes.append(point);
            }
        }
    }
    return geometryes;
}

QString Layer::layerName() const
{
    return _layerName;
}
